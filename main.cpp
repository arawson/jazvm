
#include <iostream>
#include <memory>
#include <fstream>

#include "types/object.h"
#include "types/value.h"
#include "types/table.h"
#include "types/type_primitive.h"
#include "jazvm_impl/default_lexer.h"
#include "jazvm_impl/default_parser.h"
#include "jazvm_impl/core_instructions.h"
#include "jazvm_impl/jaz.h"

using std::cout;
using std::endl;
using std::shared_ptr;
using std::string;

using namespace jazvm::types;
using namespace jazvm::lexer;
using namespace jazvm::impl;

int main(int argc, char* argv[]) {
	if (argc < 2) {
		cout << "please enter the path of the jaz file to execute as the first parameter" << endl;
		return -1;
	}
	
	char* tempName = argv[1];
	std::ifstream input(tempName, std::ios::in);
	if (!input.good()) {
		cout << "file not found " << tempName << endl;
		input.close();
		return -1;
	}
	LexerDefault lexer;
	lexer.open(input);
	
	ParserDefault parser;
	
	parser.open(&lexer);
	Object* tree = nullptr;
	
	tree = parser.getTree();
	
	input.close();
	
	Jaz jaz;
	
	jaz.load(tree);
	
	jaz.execute();
	
	return 0;
}

