
#pragma once

#include <istream>
#include <string>

namespace jazvm {
namespace lexer {

/*Implementers should split the stream into tokens, new lines should be
 * represented as a “\n” token, and the end of the stream should be
 * represented as a string containing a single end-of-text character
 * (unicode character U+0004). Implementers should also retain any extra whitespace
 * and prepend it to the next token or create a token of whitespace if the next token is on a new line.
 */

class Lexer {
public:
	const static std::string NEWLINE_TOKEN;
	const static std::string END_TOKEN;

	virtual ~Lexer();
	
	virtual void open(std::istream& input) = 0;
	
	virtual std::string nextToken() = 0;
};

}
}
