
#pragma once

#include "../types/value.h"

#include <memory>
#include <vector>

namespace jazvm {

namespace types {
class Object;
}

namespace instructions {

class Signature {
public:
	virtual ~Signature();

	virtual bool matches(const std::vector<jazvm::types::Value> objects) const = 0;
};

}
}