
#include "instruction_base.h"

namespace jazvm {
namespace instructions {

InstructionBase::InstructionBase(Signature *parameterSignature, Signature *stackSignature) :
parameterSignature(parameterSignature), stackSignature(stackSignature) {
}

InstructionBase::~InstructionBase() {
	delete stackSignature;
	delete parameterSignature;
}

bool InstructionBase::isParameterValid(const std::vector<jazvm::types::Value> &parameterList) const {
	return parameterSignature->matches(parameterList);
}

bool InstructionBase::isStackValid(const std::vector<jazvm::types::Value> &stack) const {
	return stackSignature->matches(stack);
}

}
}
