
#pragma once

#include "instruction.h"
#include "signature.h"

namespace jazvm {
namespace instructions {

class InstructionBase : public Instruction {
private:
	const Signature* const parameterSignature;
	const Signature* const stackSignature;
	
protected:
	InstructionBase(Signature * parameterSignature, Signature *stackSignature);

public:
	virtual ~InstructionBase();

	bool isParameterValid(const std::vector<jazvm::types::Value> &parameterList) const;

	bool isStackValid(const std::vector<jazvm::types::Value> &stack) const;
};

}
}