
#pragma once

#include <initializer_list>

#include "signature.h"

namespace jazvm {

namespace types {
class Type;
}

namespace instructions {

class SignatureSimple : public Signature {
private:
	const std::vector<const jazvm::types::Type*> typeList;
	
public:
	SignatureSimple(std::vector<const jazvm::types::Type*> &typeList);
	
	SignatureSimple(std::initializer_list<const jazvm::types::Type*> typeList);
	
	~SignatureSimple();

	bool matches(const std::vector<jazvm::types::Value> objects) const;
};

}
}
