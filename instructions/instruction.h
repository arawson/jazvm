
#pragma once

#include "../types/value.h"

#include <vector>
#include <memory>

namespace jazvm {

namespace types {
class Object;
class Table;
}

namespace instructions {

class Instruction {
public:
	virtual ~Instruction();

	virtual bool isStackValid(
			const std::vector<jazvm::types::Value> &stack) const = 0;

	virtual bool isParameterValid(
			const std::vector<jazvm::types::Value> &parameterList
			) const = 0;

	virtual void execute(
			std::shared_ptr<jazvm::types::Object> &vmstate,
			std::vector<jazvm::types::Value> &stack,
			std::vector<jazvm::types::Value> &parameterList
			) = 0;
};

}
}