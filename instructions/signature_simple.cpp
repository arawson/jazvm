
#include "signature_simple.h"

#include "../types/type.h"

namespace jazvm {
namespace instructions {

SignatureSimple::SignatureSimple(std::vector<const jazvm::types::Type*> &typeList) :
typeList(typeList) {
}

SignatureSimple::SignatureSimple(std::initializer_list<const jazvm::types::Type*> typeList) :
typeList(typeList) {
	
}

SignatureSimple::~SignatureSimple() {
}

bool SignatureSimple::matches(const std::vector<jazvm::types::Value> objects) const {
	bool result = true;
	
	if (typeList.size() <= objects.size()) {
		auto i = objects.end();
		for (auto j = typeList.end(); j != typeList.end(); i--, j--) {
			if (!(*j)->checkObject(*i)) {
				result = false;
				break;
			}
		}
	} else {
		result = false;
	}
	
	return result;
}

}
}
