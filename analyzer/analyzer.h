
#pragma once

namespace jazvm {

namespace types {
class Object;
}

namespace analyzer {

class Analyzer {
public:
	virtual ~Analyzer();
	
	virtual bool isValid(const jazvm::types::Object* tree) = 0;
};

}
}
