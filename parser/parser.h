
#pragma once

#include <string>
#include <stdexcept>

namespace jazvm {

namespace types {
class Object;
}

namespace lexer {
class Lexer;
}

namespace parser {

const std::string ENTRY_POINT_FIELD = "__MAIN__";

class Parser {
public:
	virtual ~Parser();
	
	virtual void open(jazvm::lexer::Lexer* lexer) = 0;
	
	virtual jazvm::types::Object* getTree() = 0;
};

class parse_error : public std::runtime_error {
public:
	parse_error(const std::string& __arg);

};

}
}
