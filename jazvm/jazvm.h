
#pragma once

#include "../types/object.h"

namespace jazvm {
namespace interface {

class JazVM : public types::Object {
public:
	virtual ~JazVM();
	
	virtual void load(Object* programData) = 0;
	
	virtual void execute() = 0;
};

}
}
