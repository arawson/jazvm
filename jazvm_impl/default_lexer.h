
#pragma once

#include "../lexer/lexer.h"

namespace jazvm {
namespace impl {

class LexerDefault : public jazvm::lexer::Lexer {
private:
	std::istream* stream;

public:
	~LexerDefault();

	void open(std::istream& input);

	std::string nextToken();
};


}
}

