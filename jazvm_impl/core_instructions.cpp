
#include "core_instructions.h"

#include <string>
#include <iostream>

#include "default_parser.h"
#include "util.h"
#include "../types/type.h"
#include "../types/object.h"
#include "../types/table.h"
#include "../types/type_primitive.h"
#include "../instructions/signature_simple.h"

using namespace jazvm::instructions;
using namespace jazvm::types;

using std::string;
using std::cout;
using std::endl;
using std::shared_ptr;

namespace jazvm {
namespace impl {

const std::string FIELD_GOTO_DEST = "__GOTO_DEST__";

shared_ptr<Object> peekScopeTable(shared_ptr<Object> vmstate) {
	auto scope = vmstate->getField(Jaz::FIELD_SUB_SCOPE);
	
	if (scope->getType() == Primitive::NIL) {
		return vmstate;
	}
	
	auto oldScope = scope;
	while ((scope = scope->getReference()->getField(Jaz::FIELD_SUB_SCOPE))->getType() != Primitive::NIL) {
		oldScope = scope;
	}
	
	return oldScope->getReference();
}

shared_ptr<Object> pushScopeTable(shared_ptr<Object> vmstate) {
	auto lastScope = peekScopeTable(vmstate);
	shared_ptr<Object> newScope(new Table());
	lastScope->putField(Jaz::FIELD_SUB_SCOPE, newScope);
	newScope->putField(Jaz::FIELD_SUB_SCOPE, Value());
	return newScope;
}

void popScopeTable(shared_ptr<Object> vmstate) {
	auto scope = vmstate->getField(Jaz::FIELD_SUB_SCOPE);
	
	if (scope->getType() == Primitive::NIL) {
		throw std::logic_error("scope popped (end) with no scope");
	}
	
	Value vmvalue(vmstate);
	
	auto oldScope = scope;
	auto olderScope = &vmvalue;
	while ((scope = scope->getReference()->getField(Jaz::FIELD_SUB_SCOPE))->getType() != Primitive::NIL) {
		olderScope = oldScope;
		oldScope = scope;
	}
	
	vmstate->putField(Jaz::FIELD_LAST_POPPED_SCOPE, *olderScope->getReference()->getField(Jaz::FIELD_SUB_SCOPE));
	olderScope->getReference()->putField(Jaz::FIELD_SUB_SCOPE, Value());
}

Value* findExistingValueInScope(shared_ptr<Object> vmstate, const string& name) {
	Value vmval(vmstate);
	Value *scope = &vmval;
	Value *ret = nullptr;
	
	auto returnCapture = vmstate->getField(Jaz::FIELD_LAST_POPPED_SCOPE);
	if (returnCapture->getType() != Primitive::NIL) {
		Value* val = returnCapture->getReference()->getField(name);
		if (val != nullptr) {
//			cout << "#found " << name << " in last popped scope " << endl;
			peekScopeTable(vmstate)->putField(name, *val);
			return val;
		}
	}
	
	do {
		auto val = scope->getReference()->getField(name);
		if (val != nullptr) {
//			cout << "#found " << name << " in scope list" << endl;
			ret = val;
		}
		scope = scope->getReference()->getField(Jaz::FIELD_SUB_SCOPE);
	} while(scope != nullptr && scope->getType() != Primitive::NIL);
	
//	if (ret == nullptr)
//		cout << "#not found " << name << endl;
	
	return ret;
}

shared_ptr<Object> newPrimitiveTable() {
	shared_ptr<Object> t(new Table());
	t->putField(OBJECT_PRIMITIVE_VALUE, 0);
	return t;
}

InstructionNop::InstructionNop() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({})
) {
}

void InstructionNop::execute(
		std::shared_ptr<jazvm::types::Object>& vmstate,
		std::vector<jazvm::types::Value>& stack,
		std::vector<jazvm::types::Value>& parameterList) {
}

InstructionCall::InstructionCall() : InstructionBase(
new SignatureSimple({&STRING_TYPE}),
new SignatureSimple({})) {
}

void InstructionCall::execute(
		std::shared_ptr<types::Object>& vmstate,
		std::vector<types::Value>& stack,
		std::vector<types::Value>& parameterList) {
	//1 make scope table
	//2 jump IP to destination

	//put return ip in scope table
	auto scope = peekScopeTable(vmstate);
	{
		//dont advance the IP here, that is the jazvm implementation's job
		Value returnIp = *(vmstate->getField(Jaz::FIELD_INSTRUCTION_POINTER));
		scope->putField(Jaz::FIELD_SCOPE_RETURN, returnIp);
	}

	auto target = parameterList[0].castToString();
	trimToken(target);
	auto parseTable = vmstate->getField(Jaz::FIELD_PARSE_ROOT)->getReference();
	auto jumpTable = parseTable->getField(TypeParseRoot::FIELD_JUMP_TABLE)->getReference();
	auto targetTable = jumpTable->getField(target);

	if (targetTable == nullptr ||
			targetTable->getType() == Primitive::NIL) {
		throw std::runtime_error(std::string("Cannot find label ").append(target));
	}

	vmstate->putField(Jaz::FIELD_INSTRUCTION_POINTER, *targetTable);
}

InstructionReturn::InstructionReturn() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({})) {
}

void InstructionReturn::execute(
		std::shared_ptr<types::Object>& vmstate,
		std::vector<types::Value>& stack,
		std::vector<types::Value>& parameterList) {
	auto scopePtr = peekScopeTable(vmstate);
	if (scopePtr == vmstate) {
		throw std::logic_error("return without call!");
	}
	auto target = scopePtr->getField(Jaz::FIELD_SCOPE_RETURN)->getReference();

	vmstate->putField(Jaz::FIELD_INSTRUCTION_POINTER, target);
}

InstructionBegin::InstructionBegin() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({})
) {
}

void InstructionBegin::execute(
		std::shared_ptr<types::Object>& vmstate,
		std::vector<types::Value>& stack,
		std::vector<types::Value>& parameterList) {

	pushScopeTable(vmstate);
}

InstructionEnd::InstructionEnd() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({})) {
}

void InstructionEnd::execute(
		std::shared_ptr<types::Object>& vmstate,
		std::vector<types::Value>& stack,
		std::vector<types::Value>& parameterList) {
	popScopeTable(vmstate);
}

InstructionPush::InstructionPush() : InstructionBase(
new SignatureSimple({&INTEGER_TYPE}),
new SignatureSimple({})
) {
}

InstructionPush::~InstructionPush() {
}

void InstructionPush::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto o = parameterList[0];
	stack.push_back(o);
}

InstructionRValue::InstructionRValue() : InstructionBase(
new SignatureSimple({&STRING_TYPE}),
new SignatureSimple({})
) {
}

void InstructionRValue::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto o = parameterList[0];
	string name = o.castToString();
	trimToken(name);
	
	auto existing = findExistingValueInScope(vmstate, name);
	if (existing == nullptr) {
		cout << name << " does not yet exist" << endl;
		auto scope = peekScopeTable(vmstate);
		existing = scope->putField(name, newPrimitiveTable());
	}
	auto value = existing->getReference()->getField(OBJECT_PRIMITIVE_VALUE);
	stack.push_back(*value);
}

InstructionLValue::InstructionLValue() : InstructionBase(
new SignatureSimple({&STRING_TYPE}),
new SignatureSimple({})
) {
}

void InstructionLValue::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto o = parameterList[0];
	string name = o.castToString();
	trimToken(name);
	
	auto existing = findExistingValueInScope(vmstate, name);
	if (existing == nullptr) {
//		cout << "create variable " << name << endl;
		auto scope = peekScopeTable(vmstate);
		existing = scope->putField(name, newPrimitiveTable());
	};
	stack.push_back(*existing);
}

InstructionPop::InstructionPop() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&ANY_TYPE})
) {
}

void InstructionPop::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	stack.pop_back();
}

InstructionAssign::InstructionAssign() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&REFERENCE_TYPE, &INTEGER_TYPE})
) {
}

void InstructionAssign::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	Value rvalue = *stack.rbegin();
	stack.pop_back();
	Value lvalue = *stack.rbegin();
	stack.pop_back();

	auto lref = lvalue.getReference();
	auto rval = rvalue.getInteger();

	lref->putField(OBJECT_PRIMITIVE_VALUE, rval);
}

InstructionCopy::InstructionCopy() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&ANY_TYPE})
) {
}

void InstructionCopy::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	stack.push_back(*stack.rbegin());
}

InstructionLabel::InstructionLabel() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({})
) {
}

void InstructionLabel::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
}

InstructionGoto::InstructionGoto() : InstructionBase(
new SignatureSimple({&ANY_TYPE}),
new SignatureSimple({})
) {
}

void InstructionGoto::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto target = parameterList[0].castToString();
	trimToken(target);
	auto parseTable = vmstate->getField(Jaz::FIELD_PARSE_ROOT)->getReference();
	auto jumpTable = parseTable->getField(TypeParseRoot::FIELD_JUMP_TABLE)->getReference();
	auto targetPtr = jumpTable->getField(target);
	if (targetPtr == nullptr) {
		throw std::runtime_error(std::string("Cannot find label ").append(target));
	}
	auto targetTable = targetPtr->getReference();

	vmstate->putField(Jaz::FIELD_INSTRUCTION_POINTER, targetTable);
}

InstructionGoFalse::InstructionGoFalse() : InstructionBase(
new SignatureSimple({&ANY_TYPE}),
new SignatureSimple({&INTEGER_TYPE})
) {
}

void InstructionGoFalse::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto go = (*stack.rbegin()).getInteger();
	stack.pop_back();
	if (go == 0) {
		auto target = parameterList.rbegin()->castToString();
		trimToken(target);
		auto parseTable = vmstate->getField(Jaz::FIELD_PARSE_ROOT)->getReference();
		auto jumpTable = parseTable->getField(TypeParseRoot::FIELD_JUMP_TABLE)->getReference();
		auto targetTable = jumpTable->getField(target);

		if (targetTable == nullptr) {
			throw std::runtime_error(std::string("Cannot find label ").append(target));
		}

		vmstate->putField(Jaz::FIELD_INSTRUCTION_POINTER, *targetTable);
	}
}

InstructionGoTrue::InstructionGoTrue() : InstructionBase(
new SignatureSimple({&ANY_TYPE}),
new SignatureSimple({&INTEGER_TYPE})
) {
}

void InstructionGoTrue::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto go = (*stack.rbegin()).getInteger();
	stack.pop_back();
	if (go != 0) {
		auto target = parameterList.rbegin()->castToString();
		trimToken(target);
		auto parseTable = vmstate->getField(Jaz::FIELD_PARSE_ROOT)->getReference();
		auto jumpTable = parseTable->getField(TypeParseRoot::FIELD_JUMP_TABLE)->getReference();
		auto targetTable = jumpTable->getField(target);

		if (targetTable == nullptr) {
			throw std::runtime_error(std::string("Cannot find label ").append(target));
		}

		vmstate->putField(Jaz::FIELD_INSTRUCTION_POINTER, *targetTable);
	}
}

InstructionHalt::InstructionHalt() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({})
) {
}

void InstructionHalt::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	vmstate->putField(Jaz::FIELD_INSTRUCTION_POINTER, Value());
}

InstructionAdd::InstructionAdd() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionAdd::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	stack.push_back(s1 + s2);
}

InstructionSub::InstructionSub() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionSub::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	stack.push_back(s2 - s1);
}

InstructionMul::InstructionMul() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionMul::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	stack.push_back(s1 * s2);
}

InstructionDiv::InstructionDiv() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionDiv::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	stack.push_back(s2 / s1);
}

InstructionMod::InstructionMod() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionMod::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	stack.push_back(s2 % s1);
}

InstructionAnd::InstructionAnd() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionAnd::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	if ((s1 != 0) && (s2 != 0)) {
		stack.push_back(1);
	} else {
		stack.push_back(0);
	}
}

InstructionOr::InstructionOr() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionOr::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	if ((s1 != 0) || (s2 != 0)) {
		stack.push_back(1);
	} else {
		stack.push_back(0);
	}
}

InstructionNot::InstructionNot() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE})
) {
}

void InstructionNot::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	if (s1 != 0) {
		stack.push_back(0);
	} else {
		stack.push_back(1);
	}
}

InstructionNotEqual::InstructionNotEqual() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionNotEqual::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	if (s1 != s2) {
		stack.push_back(1);
	} else {
		stack.push_back(0);
	}
}

InstructionLessEqual::InstructionLessEqual() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionLessEqual::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	if (s2 <= s1) {
		stack.push_back(1);
	} else {
		stack.push_back(0);
	}
}

InstructionGreaterEqual::InstructionGreaterEqual() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionGreaterEqual::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	if (s2 >= s1) {
		stack.push_back(1);
	} else {
		stack.push_back(0);
	}
}

InstructionLess::InstructionLess() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionLess::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	if (s2 < s1) {
		stack.push_back(1);
	} else {
		stack.push_back(0);
	}
}

InstructionGreater::InstructionGreater() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionGreater::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	if (s2 > s1) {
		stack.push_back(1);
	} else {
		stack.push_back(0);
	}
}

InstructionEqual::InstructionEqual() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&INTEGER_TYPE, &INTEGER_TYPE})
) {
}

void InstructionEqual::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto s1 = (*stack.rbegin()).getInteger();
	stack.pop_back();
	auto s2 = (*stack.rbegin()).getInteger();
	stack.pop_back();

	if (s2 == s1) {
		stack.push_back(1);
	} else {
		stack.push_back(0);
	}
}

InstructionPrint::InstructionPrint() : InstructionBase(
new SignatureSimple({}),
new SignatureSimple({&REFERENCE_TYPE})
) {
}

void InstructionPrint::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	auto value = (*stack.rbegin()).castToString();

	cout << value << endl;
}

InstructionShow::InstructionShow() : InstructionBase(
new SignatureSimple({&REFERENCE_TYPE}),
new SignatureSimple({})
) {
}

void InstructionShow::execute(
		std::shared_ptr<types::Object> &vmstate,
		std::vector<types::Value>&stack,
		std::vector<types::Value>&parameterList
		) {
	string output;

	if (parameterList.size() > 0) {
		if (parameterList[0].getType() == Primitive::STRING) {
			parameterList[0].getString().erase(0, 1);
		}
	}

	for (auto i = parameterList.begin(); i != parameterList.end(); i++) {
		output.append((*i).castToString());
	}

	cout << output << endl;
}

InstructionAdd::~InstructionAdd() {

}

InstructionAnd::~InstructionAnd() {

}

InstructionAssign::~InstructionAssign() {

}

InstructionCopy::~InstructionCopy() {

}

InstructionDiv::~InstructionDiv() {

}

InstructionEqual::~InstructionEqual() {

}

InstructionGoFalse::~InstructionGoFalse() {

}

InstructionGoTrue::~InstructionGoTrue() {

}

InstructionGoto::~InstructionGoto() {

}

InstructionGreater::~InstructionGreater() {

}

InstructionGreaterEqual::~InstructionGreaterEqual() {

}

InstructionHalt::~InstructionHalt() {

}

InstructionLValue::~InstructionLValue() {

}

InstructionLabel::~InstructionLabel() {

}

InstructionLess::~InstructionLess() {

}

InstructionLessEqual::~InstructionLessEqual() {

}

InstructionMod::~InstructionMod() {

}

InstructionMul::~InstructionMul() {

}

InstructionNot::~InstructionNot() {

}

InstructionNotEqual::~InstructionNotEqual() {

}

InstructionOr::~InstructionOr() {

}

InstructionPop::~InstructionPop() {

}

InstructionPrint::~InstructionPrint() {

}

InstructionRValue::~InstructionRValue() {

}

InstructionShow::~InstructionShow() {

}

InstructionSub::~InstructionSub() {

}

InstructionBegin::~InstructionBegin() {

}

InstructionEnd::~InstructionEnd() {

}

InstructionCall::~InstructionCall() {

}

InstructionReturn::~InstructionReturn() {

}

InstructionNop::~InstructionNop() {

}

}
}
