
#pragma once

#include <string>
#include <memory>
#include <istream>

#include "../types/object.h"

namespace jazvm {
namespace impl {

void trimToken(std::string& str);

void dbgTable(std::ostream &out, std::string prefix, std::shared_ptr<jazvm::types::Object> o);

void dbgTable(std::ostream &out, std::string prefix, jazvm::types::Object *o);

}
}