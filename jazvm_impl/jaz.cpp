
#include "./jaz.h"
#include "../types/table.h"
#include "../types/primitive.h"
#include "../instructions/instruction.h"
#include "default_parser.h"

#include <memory>
#include <string>
#include <vector>
#include <iostream>

using std::string;
using std::shared_ptr;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;

using jazvm::instructions::Instruction;

using namespace jazvm::types;

namespace jazvm {
namespace impl {

const string Jaz::FIELD_PARSE_ROOT = "__PARSE_ROOT__";

const string Jaz::FIELD_INSTRUCTION_POINTER = "__IP__";

const string Jaz::FIELD_SUB_SCOPE = "__MOUTHWASH__";

const string Jaz::FIELD_SCOPE_RETURN = "__SCOPE_RETURN__";

const string Jaz::FIELD_LAST_POPPED_SCOPE = "__POPPED_SCOPE__";

Jaz::Jaz() : vmstate(new Table()) {
}

Jaz::~Jaz() {
}

Value* Jaz::getField(const std::string& name) {
	return vmstate->getField(name);
}

const Value* Jaz::getField(const std::string& name) const {
	return vmstate->getField(name);
}

std::vector<std::string> Jaz::getFieldNames() const {
	return vmstate->getFieldNames();
}

bool Jaz::hasField(const std::string& name) const {
	return vmstate->hasField(name);
}

Value* Jaz::putField(const std::string& name, const Value& value) {
	return vmstate->putField(name, value);
}

void Jaz::load(Object* programData) {
	vmstate->putField(FIELD_PARSE_ROOT, Value(shared_ptr<Object>(programData)));
	
	auto root = vmstate->getField(FIELD_PARSE_ROOT)->getReference();
	vmstate->putField(FIELD_INSTRUCTION_POINTER, root->getField(TypeParseRoot::FIELD_FIRST_LINE)->getReference());
	
	vmstate->putField(FIELD_SUB_SCOPE, Value());
	vmstate->putField(FIELD_LAST_POPPED_SCOPE, Value());
}

vector<Value> loadParameters(shared_ptr<Object> ip) {
	vector<Value> parameters;
	
	auto p = ip->getField(TypeParseLine::FIELD_ARGUMENT);
	
	while (p != nullptr && p->getType() != Primitive::NIL) {
		auto d = p->getReference()->getField(TypeParseArgument::FIELD_DATA);
		
		if (d->getType() != Primitive::NIL) {
//			cout << "(parse parameter " << d->castToString() << " = ";
			
			//primitive has no type yet, it must be coerced into the correct type
			//the only inputtable types are string and int
			
			try {
				int i = std::stoi(d->castToString());
//				cout << "[int]" << i << ")" << endl;
				parameters.push_back(i);
			} catch (std::invalid_argument &ex) {
//				cout << "[string]" << d->castToString() << ")" << endl;
				parameters.push_back(Value(d->castToString()));
			}
		}
		
		p = p->getReference()->getField(TypeParseArgument::FIELD_NEXT_ARGUMENT);
	}
	
	return parameters;
}

void Jaz::execute() {
	auto ip = vmstate->getField(FIELD_INSTRUCTION_POINTER);
//	auto parseTable = vmstate->getField(FIELD_PARSE_ROOT);
	vector<Value> stack;
	
//	cout << "beginning jaz execution" << endl;
//	cout << "_______________________" << endl;
	
	while (ip->getType() != Primitive::NIL) {
		auto ipTable = ip->getReference();
//		const int lineNumber = ipTable->getField(TypeParseLine::FIELD_LINE_NUMBER)->getInteger();
		
//		cout << "IN exec line# " << lineNumber << endl;
//		cout << "(exec line# " << lineNumber << ")" << endl;
		
		vector<Value> parameters = loadParameters(ipTable);
		auto insV = ipTable->getField(TypeParseLine::FIELD_INSTRUCTION);
		
//		cout << "insv = " << insV << endl;
		if (insV != nullptr) {
			Instruction* ins = static_cast<Instruction*>(insV->getUserdata());
//			cout << " must exec on " << ins << endl;
			bool paramValid = ins->isParameterValid(parameters);
			bool stackValid = ins->isStackValid(stack);
//			cout << "stack = " << stackValid << "   param = " << paramValid << endl;
			
//			cout << "()stack has " << stack.size() << " values" << endl;
			
			if (paramValid && stackValid) {
				ins->execute(vmstate, stack, parameters);
			} else {
				throw std::logic_error("  can't execute! stack or param not valid!");
			}
		}
		
		ip = vmstate->getField(FIELD_INSTRUCTION_POINTER);
		if (ip ->getType() == Primitive::NIL) {
			break;
		}
		ip = ip->getReference()->getField(TypeParseLine::FIELD_NEXT_LINE);
		vmstate->putField(FIELD_INSTRUCTION_POINTER, *ip);
//		cout << "OUT instruction finished" << endl << endl;
	}
	
//	cout << "______________________" << endl;
//	cout << "jaz execution finished" << endl;
}

}
}

