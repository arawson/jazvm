
#include "util.h"

#include "../types/primitive.h"

using std::string;
using std::endl;
using std::istream;
using std::shared_ptr;

using namespace jazvm::types;

namespace jazvm {
namespace impl {

void trimToken(std::string& str) {
	const char *w = "\t\n\r\f\v ";
	str.erase(0, str.find_first_not_of(w));
	str.erase(str.find_last_not_of(w) + 1);
}

void dbgTable(std::ostream &out, string prefix, Object *o) {
	if (o == nullptr) {
		out << prefix << " = null!" << endl;
	} else {
		auto fields = o->getFieldNames();
		for (auto i = fields.begin(); i != fields.end(); i++) {
			auto field = o->getField(*i);
			if (field->getType() == Primitive::REFERENCE) {
				string subPrefix(prefix);
				subPrefix.append("::").append(*i);
				dbgTable(out, subPrefix, field->getReference());
			} else {
				out << prefix << "::" << *i << " = " << field->castToString() << endl;
			}
		}
	}
}

void dbgTable(std::ostream &out, string prefix, shared_ptr<Object> o) {
	dbgTable(out, prefix, o.get());
}

}
}