
#include "default_parser.h"

#include "core_instructions.h"
#include "util.h"

#include <memory>
#include <string>
#include <sstream>

#include <iostream>
using std::endl;
using std::cerr;

#include "../types/object.h"
#include "../types/table.h"
#include "../lexer/lexer.h"
#include "../parser/parser.h"

using std::shared_ptr;

using jazvm::lexer::Lexer;
using jazvm::types::Table;
using jazvm::types::Value;
using jazvm::types::Object;
using jazvm::types::Primitive;
using jazvm::parser::parse_error;
using jazvm::instructions::Instruction;

namespace jazvm {
namespace impl {

ParserDefault::ParserDefault() {
	setInstruction("begin", new InstructionBegin());
	setInstruction("end", new InstructionEnd());
	setInstruction("call", new InstructionCall());
	setInstruction("return", new InstructionReturn());
	
	setInstruction("push", new InstructionPush());
	setInstruction("rvalue", new InstructionRValue());
	setInstruction("lvalue", new InstructionLValue());
	setInstruction("pop", new InstructionPop());
	setInstruction(":=", new InstructionAssign());
	setInstruction("copy", new InstructionCopy());
	
	setInstruction("label", new InstructionLabel(), new ParserDefault::PreLabel());
	setInstruction("goto", new InstructionGoto());
	setInstruction("gofalse", new InstructionGoFalse());
	setInstruction("gotrue", new InstructionGoTrue());
	setInstruction("halt", new InstructionHalt());
	
	setInstruction("+", new InstructionAdd());
	setInstruction("-", new InstructionSub());
	setInstruction("*", new InstructionMul());
	setInstruction("/", new InstructionDiv());
	setInstruction("div", new InstructionMod());
	
	setInstruction("&", new InstructionAnd());
	setInstruction("!", new InstructionNot());
	setInstruction("|", new InstructionOr());
	
	setInstruction("<>", new InstructionNotEqual());
	setInstruction("<=", new InstructionLessEqual());
	setInstruction(">=", new InstructionGreaterEqual());
	setInstruction("<", new InstructionLess());
	setInstruction(">", new InstructionGreater());
	setInstruction("=", new InstructionEqual());
	
	setInstruction("print", new InstructionPrint());
	setInstruction("show", new InstructionShow());
}


ParserDefault::~ParserDefault() {
}

ParserDefault::Preprocessor::~Preprocessor() {
}

void ParserDefault::open(Lexer *lexer) {
	l = lexer;
}

Object* ParserDefault::getTree() {
	std::string token;

	bool firstArgumentInLine = true;
	int lines = 0;
	Object* root = new Table();
	shared_ptr<Object> line(new Table());
	shared_ptr<Object> jumps(new Table());
	shared_ptr<Object> argument(new Table());

	root->putField(TypeParseRoot::FIELD_FIRST_LINE, line);
	root->putField(TypeParseRoot::FIELD_JUMP_TABLE, jumps);

	line->putField(TypeParseLine::FIELD_ARGUMENT, argument);

	while ((token = l->nextToken()) != Lexer::END_TOKEN) {
		if (token == Lexer::NEWLINE_TOKEN) {
			//reinit
			//increment line counter
			//break line

			//apply the preprocessor to the previous line first
			if (!firstArgumentInLine) {
				auto insValue = line->getField(TypeParseLine::FIELD_INSTRUCTION);
				if (insValue->getType() == Primitive::USERDATA) {
					auto preIter = preprocessors.find(
							static_cast<Instruction*> (insValue->getUserdata()));

					if (preIter != preprocessors.end()) {
						Preprocessor *p = preIter->second;
						p->initialize(line, root);
					}
				} else {
					cerr << "line " << lines+1 << ": warning: line without valid instruction" << endl;
				}
			}

			shared_ptr<Object> line2(new Table());
			line->putField(TypeParseLine::FIELD_NEXT_LINE, line2);
			line->putField(TypeParseLine::FIELD_LINE_NUMBER, lines + 1);

			argument->putField(TypeParseArgument::FIELD_DATA, Value());
			argument = shared_ptr<Object>(new Table());
			line2->putField(TypeParseLine::FIELD_ARGUMENT, argument);

			line = line2;

			firstArgumentInLine = true;
			lines++;
		} else {
			std::string tokenTrimmed(token);
			trimToken(tokenTrimmed);
			
			if (firstArgumentInLine) {
				firstArgumentInLine = false;
				
				if (hasInstruction(tokenTrimmed)) {
					Instruction* ins = getInstruction(tokenTrimmed);
					line->putField(TypeParseLine::FIELD_INSTRUCTION, Value(static_cast<void*> (ins)));
				} else {
					std::cerr << "line " << lines << ": warning: unknown instruction: \""
							<< tokenTrimmed << "\"" << std::endl;
					line->putField(TypeParseLine::FIELD_INSTRUCTION, Value());
				}
			} else {
				//parse the string into something
				//at the moment, jazvm can only support 3 parameter types
				//variable names, integers, and comment strings
				//so if it cant be parsed as an integer, it must be a comment or
				//a variable
				//TODO: make a small module to handle this

				std::istringstream ss(tokenTrimmed);
				int i;
				if (!(ss >> i)) {
					argument->putField(TypeParseArgument::FIELD_DATA, token);
				} else {
					argument->putField(TypeParseArgument::FIELD_DATA, i);
				}

				//advance argument by one
				shared_ptr<Object> arg(new Table());
				argument->putField(TypeParseArgument::FIELD_NEXT_ARGUMENT, arg);
				argument = arg;
			}
		}
	}

	root->putField(TypeParseRoot::FIELD_NUM_LINES, lines);

	return root;
}

void ParserDefault::PreLabel::initialize(
		std::shared_ptr<jazvm::types::Object>& line,
		jazvm::types::Object* root) {
	auto jumpTable = root->getField(TypeParseRoot::FIELD_JUMP_TABLE)->getReference();
	auto argument = line->getField(TypeParseLine::FIELD_ARGUMENT)->getReference();
	auto label = argument->getField(TypeParseArgument::FIELD_DATA)->castToString();
	trimToken(label);
	jumpTable->putField(label, line);
}

bool ParserDefault::hasInstruction(const std::string& mnemonic) const {
	auto i = instructions.find(mnemonic);

	return i != instructions.end();
}

jazvm::instructions::Instruction* ParserDefault::getInstruction(const std::string &mnemonic) {

	return instructions[mnemonic];
}

void ParserDefault::setInstruction(const std::string &mnemonic, Instruction* instruction) {
	if (hasInstruction(mnemonic)) {

		throw std::logic_error("Instruction added over another instruction's mnemonic.");
	}
	instructions[mnemonic] = instruction;
}

void ParserDefault::setInstruction(
		const std::string& mnemonic,
		jazvm::instructions::Instruction* instruction,
		Preprocessor* op) {

	setInstruction(mnemonic, instruction);
	preprocessors[instruction] = op;
}

const std::string TypeParseLine::FIELD_ARGUMENT = "__ARGUMENT__";
const std::string TypeParseLine::FIELD_LINE_NUMBER = "__LINE_NUMBER__";
const std::string TypeParseLine::FIELD_NEXT_LINE = "__NEXT_LINE__";
const std::string TypeParseLine::FIELD_INSTRUCTION = "__INSTRUCTION__";

bool TypeParseLine::checkObject(std::shared_ptr<jazvm::types::Object> o) const {

	return
	o->getField(FIELD_ARGUMENT)->getType() == Primitive::REFERENCE &&
			o->getField(FIELD_NEXT_LINE)->getType() == Primitive::REFERENCE &&
			o->getField(FIELD_LINE_NUMBER)->getType() == Primitive::INTEGER &&
			o->getField(FIELD_INSTRUCTION)->getType() == Primitive::USERDATA;
}

const std::string TypeParseArgument::FIELD_DATA = "__DATA__";
const std::string TypeParseArgument::FIELD_NEXT_ARGUMENT = "__NEXT_ARGUMENT__";

bool TypeParseArgument::checkObject(std::shared_ptr<jazvm::types::Object> o) const {

	return
	o->hasField(FIELD_DATA) &&
			o->getField(FIELD_NEXT_ARGUMENT)->getType() == Primitive::REFERENCE;
}

const std::string TypeParseRoot::FIELD_FIRST_LINE = "__MAIN__";
const std::string TypeParseRoot::FIELD_JUMP_TABLE = "__JUMP_TABLE__";
const std::string TypeParseRoot::FIELD_NUM_LINES = "__PROGRAM_LENGTH__";

bool TypeParseRoot::checkObject(std::shared_ptr<jazvm::types::Object> o) const {
	return
	o->getField(FIELD_FIRST_LINE)->getType() == Primitive::REFERENCE &&
			o->getField(FIELD_JUMP_TABLE)->getType() == Primitive::REFERENCE &&
			o->getField(FIELD_NUM_LINES)->getType() == Primitive::INTEGER;
}

}
}

