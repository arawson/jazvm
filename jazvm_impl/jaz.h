
#pragma once

#include <string>
#include <memory>

#include "../jazvm/jazvm.h"

namespace jazvm {
namespace impl {

class Jaz : public jazvm::interface::JazVM {
public:
	static const std::string FIELD_PARSE_ROOT;
	static const std::string FIELD_INSTRUCTION_POINTER;
	static const std::string FIELD_SUB_SCOPE;
	static const std::string FIELD_SCOPE_RETURN;
	static const std::string FIELD_LAST_POPPED_SCOPE;

private:
	std::shared_ptr<Object> vmstate;

public:
	Jaz();
	~Jaz();

	void execute();

	void load(Object* programData);

	jazvm::types::Value* getField(const std::string& name);

	const jazvm::types::Value* getField(const std::string& name) const;

	bool hasField(const std::string& name) const;

	jazvm::types::Value* putField(const std::string& name, const jazvm::types::Value& value);

	std::vector<std::string> getFieldNames() const;
};

}
}
