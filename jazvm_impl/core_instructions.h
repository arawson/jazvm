
#pragma once

#include <string>
#include <memory>

#include "jaz.h"
#include "../instructions/instruction.h"
#include "../instructions/instruction_base.h"

namespace jazvm {
namespace impl {

extern const std::string FIELD_GOTO_DEST;

class InstructionNop : public instructions::InstructionBase {
public:
	InstructionNop();
	~InstructionNop();

	void execute(
			std::shared_ptr<jazvm::types::Object>& vmstate,
			std::vector<jazvm::types::Value>& stack,
			std::vector<jazvm::types::Value>& parameterList
			);

};

class InstructionBegin : public instructions::InstructionBase {
public:
	InstructionBegin();
	~InstructionBegin();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionEnd : public instructions::InstructionBase {
public:
	InstructionEnd();
	~InstructionEnd();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionCall : public instructions::InstructionBase {
public:
	InstructionCall();
	~InstructionCall();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionReturn : public instructions::InstructionBase {
public:
	InstructionReturn();
	~InstructionReturn();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionPush : public instructions::InstructionBase {
public:
	InstructionPush();
	~InstructionPush();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionRValue : public instructions::InstructionBase {
public:
	InstructionRValue();
	~InstructionRValue();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionLValue : public instructions::InstructionBase {
public:
	InstructionLValue();
	~InstructionLValue();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionPop : public instructions::InstructionBase {
public:
	InstructionPop();
	~InstructionPop();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionAssign : public instructions::InstructionBase {
public:
	InstructionAssign();
	~InstructionAssign();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionCopy : public instructions::InstructionBase {
public:
	InstructionCopy();
	~InstructionCopy();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionLabel : public instructions::InstructionBase {
public:
	InstructionLabel();
	~InstructionLabel();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionGoto : public instructions::InstructionBase {
public:
	InstructionGoto();
	~InstructionGoto();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionGoFalse : public instructions::InstructionBase {
public:
	InstructionGoFalse();
	~InstructionGoFalse();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionGoTrue : public instructions::InstructionBase {
public:
	InstructionGoTrue();
	~InstructionGoTrue();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionHalt : public instructions::InstructionBase {
public:
	InstructionHalt();
	~InstructionHalt();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionAdd : public instructions::InstructionBase {
public:
	InstructionAdd();
	~InstructionAdd();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionSub : public instructions::InstructionBase {
public:
	InstructionSub();
	~InstructionSub();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionMul : public instructions::InstructionBase {
public:
	InstructionMul();
	~InstructionMul();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionDiv : public instructions::InstructionBase {
public:
	InstructionDiv();
	~InstructionDiv();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionMod : public instructions::InstructionBase {
public:
	InstructionMod();
	~InstructionMod();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionAnd : public instructions::InstructionBase {
public:
	InstructionAnd();
	~InstructionAnd();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionNot : public instructions::InstructionBase {
public:
	InstructionNot();
	~InstructionNot();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionOr : public instructions::InstructionBase {
public:
	InstructionOr();
	~InstructionOr();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionNotEqual : public instructions::InstructionBase {
public:
	InstructionNotEqual();
	~InstructionNotEqual();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionLessEqual : public instructions::InstructionBase {
public:
	InstructionLessEqual();
	~InstructionLessEqual();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionGreaterEqual : public instructions::InstructionBase {
public:
	InstructionGreaterEqual();
	~InstructionGreaterEqual();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionLess : public instructions::InstructionBase {
public:
	InstructionLess();
	~InstructionLess();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionGreater : public instructions::InstructionBase {
public:
	InstructionGreater();
	~InstructionGreater();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionEqual : public instructions::InstructionBase {
public:
	InstructionEqual();
	~InstructionEqual();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionPrint : public instructions::InstructionBase {
public:
	InstructionPrint();
	~InstructionPrint();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

class InstructionShow : public instructions::InstructionBase {
public:
	InstructionShow();
	~InstructionShow();

	void execute(
			std::shared_ptr<types::Object> &vmstate,
			std::vector<types::Value> &stack,
			std::vector<types::Value> &parameterList
			);
};

}
}
