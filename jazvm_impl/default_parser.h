
#pragma once

#include "../lexer/lexer.h"
#include "../parser/parser.h"
#include "../instructions/instruction.h"
#include "../types/type.h"

#include <unordered_map>

namespace jazvm {
namespace impl {

class ParserDefault : public jazvm::parser::Parser {
private:
	jazvm::lexer::Lexer* l;

	std::unordered_map<std::string, jazvm::instructions::Instruction*> instructions;
public:

	class Preprocessor {
	public:
		virtual ~Preprocessor();

		virtual void initialize(
				std::shared_ptr<jazvm::types::Object>& line,
				jazvm::types::Object* root
				) = 0;
	};

	class PreLabel : public Preprocessor {
	public:
		void initialize(
				std::shared_ptr<jazvm::types::Object>& line,
				jazvm::types::Object* root
				);
	};
	
	ParserDefault();

	~ParserDefault();

	void open(jazvm::lexer::Lexer* lexer);

	jazvm::types::Object* getTree();

	void setInstruction(const std::string &mnemonic, jazvm::instructions::Instruction* instruction);

	void setInstruction(const std::string &mnemonic, jazvm::instructions::Instruction* instruction, Preprocessor* op);

	bool hasInstruction(const std::string& mnemonic) const;

	jazvm::instructions::Instruction* getInstruction(const std::string &mnemonic);

private:
	PreLabel preLabel;
	std::unordered_map<jazvm::instructions::Instruction*, Preprocessor*> preprocessors;
};

class TypeParseRoot : public jazvm::types::Type {
public:
	static const std::string FIELD_FIRST_LINE;
	static const std::string FIELD_NUM_LINES;
	static const std::string FIELD_JUMP_TABLE;

	bool checkObject(std::shared_ptr<jazvm::types::Object> o) const;

	bool operator==(const Type& other) const;
};

class TypeParseLine : public jazvm::types::Type {
public:
	static const std::string FIELD_NEXT_LINE;
	static const std::string FIELD_LINE_NUMBER;
	static const std::string FIELD_ARGUMENT;
	static const std::string FIELD_INSTRUCTION;

	bool checkObject(std::shared_ptr<jazvm::types::Object> o) const;

	bool operator==(const Type& other) const;
};

class TypeParseArgument : public jazvm::types::Type {
public:
	static const std::string FIELD_NEXT_ARGUMENT;
	static const std::string FIELD_DATA;

	bool checkObject(std::shared_ptr<jazvm::types::Object> o) const;

	bool operator==(const Type& other) const;
};

}
}

