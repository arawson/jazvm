
#include "default_lexer.h"

namespace jazvm {
namespace impl {

void LexerDefault::open(std::istream &input) {
	stream = &input;
}

LexerDefault::~LexerDefault() {}

/*Implementers should split the stream into tokens, new lines should be represented as a “\n” token, and the end of the stream should be represented as a string containing a single end-of-text character (unicode character U+0004). Implementers should also retain any extra whitespace and prepend it to the next token or create a token of whitespace if the next token is on a new line.
 */

std::string LexerDefault::nextToken() {
	std::string token = "";

	if (stream->eof()) return "\u0004";

	bool hasNonWhitespace = false;
	bool done = false;

	do {
		char c;
		stream->get(c);
		
		switch (c) {
			case '\r':
				if (!stream->eof()) stream->get(c);
			case '\n':
				if (token.length() > 0) {
					//this is an EOL after a valid token
					//break token and rewind 1 character
					done = true;
					stream->unget();
				} else {
					//this eol should be its own token
					token = Lexer::NEWLINE_TOKEN;
					done = true;
				}
				break;

			case ' ':
			case 0x09: //tab
				if (hasNonWhitespace) {
					//break the token
					done = true;
					stream->unget();
				} else {
					//token still is only whitespace
					token.append(&c, 1);
				}
				break;

			default:
				//other character, our spec says not to break here
				//so just append it to the token
				hasNonWhitespace = true;
				token.append(&c, 1);
				break;
		}
		
	} while (!stream->eof() && !done);

	return token;
}

}
}

