
#include "type_primitive.h"

#include "object.h"

namespace jazvm {
namespace types {

TypePrimitive::TypePrimitive(Primitive primitive) : primitive(primitive) {
}

bool TypePrimitive::operator==(const Type& t) const {
	bool result = false;
	const TypePrimitive *tp = dynamic_cast<const TypePrimitive*>(&t);
	if (tp != nullptr) {
		result = (tp->primitive == this->primitive);
	}
	return result;
}

bool TypePrimitive::checkObject(const Value& o) const {
	bool result = false;
	if (o.getType() == Primitive::REFERENCE) {
		auto o2 = o.getReference();
		const Value* field = o2->getField(OBJECT_PRIMITIVE_VALUE);
		if (field != nullptr) {
			result = (field->getType() == this->primitive);
		}
	} else if (o.getType() == this->primitive) {
		result = true;
	}
	return result;
}

const TypePrimitive REFERENCE_TYPE = TypePrimitive(Primitive::REFERENCE);
const TypePrimitive NIL_TYPE = TypePrimitive(Primitive::NIL);
const TypePrimitive INTEGER_TYPE = TypePrimitive(Primitive::INTEGER);
const TypePrimitive STRING_TYPE = TypePrimitive(Primitive::STRING);
const TypePrimitive USERDATA_TYPE = TypePrimitive(Primitive::USERDATA);

}
}

