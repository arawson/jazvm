
#pragma once

#include <string>

namespace jazvm {
namespace types {

enum class Primitive : int {
	REFERENCE,
	NIL,
	INTEGER,
	STRING,
	USERDATA,
};

const std::string OBJECT_PRIMITIVE_VALUE = "__VALUE__";

}
}

