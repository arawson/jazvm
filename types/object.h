
#pragma once

#include <string>
#include <vector>
#include <memory>

#include "value.h"
#include "type.h"

namespace jazvm {
namespace types {

class Object {
public:
	virtual ~Object();

//	virtual Type* getType() const = 0;

	virtual Value* getField(const std::string &name) = 0;

	virtual const Value* getField(const std::string &name) const = 0;
	
	virtual bool hasField(const std::string& name) const = 0;

	virtual Value* putField(const std::string &name, const Value& value) = 0;
	
	virtual std::vector<std::string> getFieldNames() const = 0;
};

}
}

