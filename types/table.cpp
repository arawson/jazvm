
#include "table.h"

#include "primitive.h"

namespace jazvm {
namespace types {

std::shared_ptr<Object> Table::getPrimitive(Value& value) {
	std::shared_ptr<Object> o(new Table());
	o->putField(OBJECT_PRIMITIVE_VALUE, value);
	return o;
}

Table::~Table() {}

Value* Table::getField(const std::string &name) {
	Value* value;
	if (data.count(name) == 0) {
		value = nullptr;
	} else {
		value = &data[name];
	}
	return value;
}

const Value* Table::getField(const std::string &name) const {
	if (data.count(name) == 0) {
		return nullptr;
	} else {
		return &data.at(name);
	}
}

Value* Table::putField(const std::string &name, const Value& value) {
	data[name] = value;
	return &data[name];
}

bool Table::hasField(const std::string& name) const {
	auto i = data.find(name);
	return i != data.end();
}

std::vector<std::string> Table::getFieldNames() const {
	std::vector<std::string> names;
	for (auto i = data.begin(); i != data.end(); i++) {
		names.push_back(i->first);
	}
	return names;
}


}
}

