
#pragma once

#include <unordered_map>
#include <string>
#include <memory>

#include "object.h"
#include "value.h"

namespace jazvm {
namespace types {

class Table : public Object {
private:
	std::unordered_map<std::string, Value> data;

public:
	virtual ~Table();

	virtual Value* getField(const std::string &name);

	virtual const Value* getField(const std::string &name) const;

	virtual bool hasField(const std::string& name) const;

	virtual Value* putField(const std::string &name, const Value& value);
	
	static std::shared_ptr<Object> getPrimitive(Value& val);

	virtual std::vector<std::string> getFieldNames() const;

};

}
}

