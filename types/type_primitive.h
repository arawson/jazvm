
#pragma once

#include <string>
#include <memory>

#include "primitive.h"
#include "type.h"

namespace jazvm {
namespace types {

class TypePrimitive : public Type {
private:
	const Primitive primitive;
public:
	TypePrimitive(Primitive primitive);

	bool operator==(const Type& t) const;

	bool checkObject(const Value& o) const;
};

extern const TypePrimitive REFERENCE_TYPE;
extern const TypePrimitive NIL_TYPE;
extern const TypePrimitive INTEGER_TYPE;
extern const TypePrimitive STRING_TYPE;
extern const TypePrimitive USERDATA_TYPE;

}
}
