
#include "type.h"

namespace jazvm {
namespace types {

Type::~Type() {
}

bool TypeAny::checkObject(const Value& o) const {
	return true;
}

bool TypeAny::operator==(const Type& other) const {
	return &other == this;
}

const TypeAny ANY_TYPE;

}
}

