
#include "value.h"
#include "type_except.h"

#include <stdexcept>
#include <sstream>

namespace jazvm {
namespace types {

Value::Value() : type(Primitive::NIL), reference(nullptr) {}

Value::Value(int v) : type(Primitive::INTEGER), integer(v) {}

Value::Value(std::shared_ptr<Object> ref) : type(Primitive::REFERENCE),
		reference(ref) {}

Value::Value(std::string str) : type(Primitive::STRING), string(str) {}

Value::Value(const char* str) : type(Primitive::STRING), string(str) {}

Value::Value(void* user) : type(Primitive::USERDATA), userdata(user) {}

Value::~Value() {
	switch (type) {
		case Primitive::USERDATA:
		case Primitive::INTEGER:
			break;
		case Primitive::NIL:
		case Primitive::REFERENCE:
			reference.~shared_ptr();
			break;
		case Primitive::STRING:
			string.~basic_string();
			break;
		default:
			throw std::runtime_error("destructor not implemented yet");
			break;
	}
}

int& Value::getInteger() {
	if (type != Primitive::INTEGER) {
		throw cast_error("Value is not of integer type.");
	}
	return integer;
}

int Value::getInteger() const {
	if (type != Primitive::INTEGER) {
		throw cast_error("Value is not of integer type.");
	}
	return integer;
}

std::shared_ptr<Object>& Value::getReference() {
	if (type != Primitive::REFERENCE) {
		throw cast_error("Value is not of reference type.");
	}
	return reference;
}

std::shared_ptr<Object> Value::getReference() const {
	if (type != Primitive::REFERENCE) {
		throw cast_error("Value is not of reference type.");
	}
	return reference;
}

std::string& Value::getString() {
	if (type != Primitive::STRING) {
		throw cast_error("Value is not of string type.");
	}
	return string;
}

std::string Value::getString() const {
	if (type != Primitive::STRING) {
		throw cast_error("Value is not of string type.");
	}
	return string;
}

void*& Value::getUserdata() {
	if (type != Primitive::USERDATA) {
		throw cast_error("Value is not of userdata type.");
	}
	return userdata;
}

void* Value::getUserdata() const {
	if (type != Primitive::USERDATA) {
		throw cast_error("Value is not of userdata type.");
	}
	return userdata;
}

Value& Value::operator=(const Value& right) {
	// Check for self-assignment!
	if (this == &right) // Same object?
		return *this; // Yes, so skip assignment, and just return *this.
	
	this->~Value();
	type = right.type;
	switch (type) {
		case Primitive::INTEGER:
			integer = right.integer;
			break;
		case Primitive::USERDATA:
			userdata = right.userdata;
			break;
		case Primitive::NIL:
		case Primitive::REFERENCE:
			new (&reference) std::shared_ptr<Object>(right.reference);
			break;
		case Primitive::STRING:
			new (&string) std::string(right.string);
			break;
		default:
			throw std::runtime_error("assign not implemented yet");
			break;
	}
	
	return *this;
}

Value::Value(const Value& other) : type(other.type) {
	switch(type) {
		case Primitive::INTEGER:
			integer = other.integer;
			break;
		case Primitive::USERDATA:
			userdata = other.userdata;
			break;
		case Primitive::NIL:
		case Primitive::REFERENCE:
			new (&reference) std::shared_ptr<Object>(other.reference);
			break;
		case Primitive::STRING:
			new (&string) std::string(other.string);
			break;
		default:
			throw std::runtime_error("copy not implemented yet");
			break;
	}
}

Primitive Value::getType() const {
	return type;
}

std::string Value::castToString() const {
	std::stringstream ss;
	switch (type) {
		case Primitive::INTEGER:
			ss << integer;
			break;
		case Primitive::USERDATA:
			ss << userdata;
			break;
		case Primitive::REFERENCE:
			ss << reference.get();
			break;
		case Primitive::STRING:
			ss << string;
			break;
		case Primitive::NIL:
			ss << "NIL";
			break;
		default:
			throw std::runtime_error("cast not implemented yet");
	}
	return ss.str();
}

}
}

