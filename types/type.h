
#pragma once

#include <memory>

namespace jazvm {
namespace types {

class Value;

class Type {
public:
	virtual ~Type();

	virtual bool operator==(const Type& other) const = 0;

	virtual bool checkObject(const Value& o) const = 0;
};

class TypeAny : public Type {
public:	
	bool operator==(const Type& other) const;
	
	bool checkObject(const Value& o) const;
};

extern const TypeAny ANY_TYPE;

}
}

