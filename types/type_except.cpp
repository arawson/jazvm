
#include "type_except.h"

namespace jazvm {
	namespace types {

		cast_error::cast_error(const std::string& __arg) :
		runtime_error(__arg) {
		}

	}
}