
#pragma once

#include <memory>

#include "primitive.h"

namespace jazvm {
namespace types {

class Object;

class Value {
private:
	Primitive type;
	union {
		int integer;
		std::shared_ptr<Object> reference;
		std::string string;
		void* userdata;
	};

public:
	Value();
	
	~Value();
	
	Value(const Value& other);
	
	Value(int v);
	
	Value(std::shared_ptr<Object> ref);
	
	Value(std::string str);
	
	Value(const char* str);
	
	explicit Value(void* user);
	
	Value& operator=(const Value& right);

	Primitive getType() const;
	
	int& getInteger();
	
	int getInteger() const;
	
	std::shared_ptr<Object>& getReference();
	
	std::shared_ptr<Object> getReference() const;
	
	std::string& getString();
	
	std::string getString() const;
	
	void*& getUserdata();
	
	void* getUserdata() const;
	
	std::string castToString() const;
};

}
}

