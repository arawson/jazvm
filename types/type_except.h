
#pragma once

#include <stdexcept>
#include <string>

namespace jazvm {
namespace types {

class cast_error : public std::runtime_error {
public:
			cast_error(const std::string& __arg);

};
	
}
}
